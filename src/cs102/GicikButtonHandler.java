package cs102;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class GicikButtonHandler extends MouseAdapter {
    public void mouseEntered(MouseEvent mouseEvent) {
        if (mouseEvent.getSource() instanceof JButton) {
            Random random = new Random();
            ((JButton) mouseEvent.getSource()).setLocation(random.nextInt(350), random.nextInt(350));
        }
    }
}

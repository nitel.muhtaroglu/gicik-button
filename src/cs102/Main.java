package cs102;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Gicik Button");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);

        frame.setLayout(null);
        JButton button = new JButton("Catch me!");
        button.setBounds(200, 200, 100, 30);

        button.addMouseListener(new GicikButtonHandler());

        frame.add(button);
        frame.setVisible(true);
    }
}
